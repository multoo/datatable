<?php

namespace Multoo\DataTable;

abstract class AbstractHelper implements HelperInterface
{

    protected $columns;
    protected $extraFields = "";
    protected $where = "";
    protected $limit = "";
    protected $order = "";
    protected $data = array();
    protected $rows = array();
    protected $totalDisplayRecords = 0;
    protected $total = 0;

    public function setColumns($columns)
    {
        if (isset($columns['extra']) || isset($columns['table'])) {
            if (isset($columns['extra'])) {
                $this->extraFields = $columns['extra'];
            }
            $this->columns = $columns['table'];
        } else {
            $this->columns = $columns;
        }

        return $this;
    }

    abstract protected function rows();

    public function json()
    {
        $this->rows();

        $output = array(
            "sEcho" => intval($_GET['sEcho'] ?? 1),
            "iTotalRecords" => $this->total,
            "iTotalDisplayRecords" => $this->totalDisplayRecords,
            "aaData" => $this->rows
        );

        return json_encode($output);
    }
}
