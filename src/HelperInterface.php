<?php

namespace Multoo\DataTable;

interface HelperInterface
{

    public function setColumns($columns);

    public function json();
}
