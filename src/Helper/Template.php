<?php

namespace Multoo\DataTable\Helper;

class Template
{
    /**
     * @var \Twig\Environment
     */
    protected static $twigEnvironment = null;

    /**
     * @var array
     */
    protected static $cache = [];

    /**
     * @param \Twig\Environment $twigEnvironment
     */
    public static function setTwig(\Twig\Environment $twigEnvironment)
    {
        self::$twigEnvironment = $twigEnvironment;
    }

    public static function clearCache()
    {
        self::$cache = [];
    }

    /**
     * @param $columnName
     * @param $template
     * @param $lineOrEntity
     *
     * @return mixed
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public static function parseRow($columnName, $template, &$lineOrEntity)
    {
        if (strpos($template, '<?=') !== false || strpos($template, '{{') !== false || strpos($template, '{%') !== false) {
            $output = $template;

            $withPhp = false;
            if (strpos($output, '<?=') !== false) {
                $stringParser = new \Library\PhpStringParser($lineOrEntity);
                $output = $stringParser->parse($template);
                $withPhp = true;
            }

            if (strpos($output, '{{') !== false || strpos($output, '{%') !== false) {
                if (self::$twigEnvironment === null) {
                    throw new \Exception('Twig env. not set');
                }

                if ($withPhp === true || !isset(self::$cache[$columnName])) {
                    self::$cache[$columnName] = self::$twigEnvironment->createTemplate($output);
                }

                $output = self::$cache[$columnName]->render([(is_array($lineOrEntity) ? 'line' : 'entity') => $lineOrEntity]);
            }

            return $output;
        } else {
            if (is_array($lineOrEntity) && !empty($lineOrEntity)) {
                foreach ($lineOrEntity as $varName => $varValue) {
                    ${$varName} = $varValue;
                }
            }
            eval('$returnBlock = "' . addcslashes($template, '"') . '";');
            return $returnBlock;
        }
    }
}
