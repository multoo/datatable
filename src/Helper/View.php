<?php

namespace Multoo\DataTable\Helper;

use Multoo\DataTable\Helper\Template;

class View
{
    /**
     * @param $id
     * @param array $cols
     * @param null $ajaxSrcUrl
     * @param null $defaultLength
     * @param array $lengthMenu
     * @param null $tableName
     * @return string
     */
    public static function js($id, array $cols, $ajaxSrcUrl = null, $defaultLength = null, array $lengthMenu = [25, 50, 100, 250, 500, 1000], $tableName = null, $sortTable = true)
    {
        $output = '
        <script type="text/javascript">
            var ' . $id . '_dataTable;
            
            $(document).ready(function () {
                ' . $id . '_dataTable = $(\'#' . $id . '\').dataTable({';

        if (isset($ajaxSrcUrl)) {
            $output .= 'sortClasses: false,
                    processing: true,
                    serverSide: true,
                    drawCallback: function (settings) {
                        if (typeof html5WorkARounds == \'function\') {
                            html5WorkARounds();
                        }
                    },
                    ajaxSource: "' . $ajaxSrcUrl . '",';
        }

        $output .= 'autoWidth: false,
                    dom: \'C<"clear">lfr<"dataTableOverflowContainer"t>ipB<"clear spacer">T\',
                    "oColVis": {
                        "buttonText": "Kolommen",
                        fnLabel: function (index, title, th) {
                            if (index == 0 && title == \'\') {
                                title = \'Opties\';
                            }
                            return (index + 1) + \'. \' + title;
                        }
                    },
                    buttons: [
                        {
                            extend: \'copy\',
                            exportOptions: {
                                columns: \':visible\'
                            }
                        },
                        {
                            extend: \'csv\',
                            charset: \'utf-8\',
                            extension: \'.csv\',
                            fieldSeparator: \';\',
                            fieldBoundary: \'\',
                            bom: true,
                            title: \'' . ($tableName ?? 'Table #' . $id) . '\',
                            exportOptions: {
                                columns: \':visible\'
                            }
                        },
                        {
                            extend: \'pdf\',
                            title: \'' . ($tableName ?? 'Table #' . $id) . '\',
                            exportOptions: {
                                columns: \':visible\'
                            },
                            customize: function (doc) {
                                doc.styles.tableHeader = {
                                    bold: true,
                                    fontSize: 9,
                                    color: \'white\',
                                    fillColor: \'#333\',
                                    alignment: \'left\'
                                };
                                doc.defaultStyle.fontSize = 9;
                            }
                        }
                    ],
                    ' . (!empty($defaultLength) && in_array($defaultLength, $lengthMenu) ? 'displayLength: ' . $defaultLength . ', ' : null) . '
                    lengthMenu: [[' . str_replace('all', '-1', strtolower(implode(', ', $lengthMenu))) . '], [\'' . implode('\', \'', $lengthMenu) . '\']],
                    stateSave: false,';

        if ($sortTable == false) {
            $output .= "bSort: false,";
        }

        $orderParts = [];
        foreach ($cols as $colNr => $col) {
            if (isset($col['order'])) {
                if (is_array($col['order']) && count($col['order']) === 2) {
                    $orderParts[$col['order'][0]] = '[' . $colNr . ', "' . strtolower($col['order'][1] ?? 'asc') . '"]';
                } else {
                    $orderParts[] = '[' . $colNr . ', "' . strtolower($col['order'] ?? 'asc') . '"]';
                }
            }
        }

        if (count($orderParts) > 0) {
            $output .= '
                order: [' . implode(', ', $orderParts) . '],
            ';
        }

        $output .= '
            columns: [
        ';

        $colProps = [];
        foreach ($cols as $col) {
            $tmp = [];
            if (isset($col['sortable']) && $col['sortable'] === false) {
                $tmp['bSortable'] = false;
            }

            if (isset($col['visible']) && $col['visible'] === false) {
                $tmp['bVisible'] = false;
            }

            if (isset($col['type'])) {
                if ($col['type'] == 'datetime') {
                    $tmp['sType'] = 'datetime-nl';
                } elseif ($col['type'] == 'date') {
                    $tmp['sType'] = 'date-nl';
                } elseif ($col['type'] == 'float') {
                    $tmp['sType'] = 'numeric-comma';
                } elseif ($col['type'] == 'float') {
                    $tmp['sType'] = 'dom-checkbox';
                }
            }
            $colProps[] = !empty($tmp) ? json_encode($tmp) : 'null';
        }

        $output .= implode(', ', $colProps);

        $output .= '
                    ]
                });
                
                $(\'#' . $id . 'Filter input[colPos], #' . $id . 'Filter select[colPos]\').keyup(function () {
                    var $this = jQuery(this);
                    if (typeof dataTableSearchTimer !== "undefined") {
                        clearTimeout(dataTableSearchTimer);
                    }
        
                    dataTableSearchTimer = setTimeout(function () {
                        ' . $id . '_dataTable.fnFilter($this.val(), $this.attr("colPos"));
                    }, 250);
                });
        
                $(\'#' . $id . 'Filter select[colPos]\').change(function () {
                    ' . $id . '_dataTable.fnFilter($(this).val(), $(this).attr("colPos"));
                });
        
                $(\'#' . $id . 'Filter input[colPos], #' . $id . 'Filter select[colPos]\').each(function () {
                    if ($(this).val() != \'\') {
                        var ' . $id . '_settings = ' . $id . '_dataTable.fnSettings();
                        ' . $id . '_settings.aoPreSearchCols[$(this).attr("colPos")].sSearch = $(this).val();
                    }
                });
            }); 
        </script>
        ';

        return $output;
    }

    /**
     * @param $id
     * @param array $cols
     * @param \Traversable|string|null $data
     * @return string
     */
    public static function html($id, array $cols, $data = null)
    {
        Template::clearCache();

        $output = '
        <table class="dataTable" id="' . $id . '">
            <thead>
            <tr>
        ';

        foreach ($cols as $col) {
            $output .= '<th>' . ($col['title'] ?? null) . '</th>';
        }

        $output .= '
            </tr>
            </thead>
            <tbody>';

        if (isset($data) && $data instanceof \Traversable) {
            foreach ($data as $line) {
                $output .= '<tr>';
                foreach ($cols as $colNr => $col) {
                    $output .= '<td>';
                    if (isset($col['template'])) {
                        $output .= Template::parseRow(($col['title'] ?? 'Col' . $colNr), $col['template'], $line);
                    } else {
                        $output .= (is_array($line) ? $line[$col['field']] : $line->{$col['field']});
                    }
                    $output .= '</td>';
                }
                $output .= '</tr>';
            }
        }

        $output .= '
            </tbody>
        </table>
        ';
        return $output;
    }

    /**
     * @param $id
     * @param array $cols
     * @param \Traversable|string|null $data
     * @param null $defaultLength
     * @param array $lengthMenu
     * @param $tableName
     * @return string
     */
    public static function build($id, array $cols, $data = null, $defaultLength = null, array $lengthMenu = [25, 50, 100, 250, 500, 1000], $tableName = null, $sortTable = true)
    {
        $ajaxSrcUrl = null;

        if (is_string($data)) {
            $ajaxSrcUrl = $data;
            $data = [];
        }

        $output = self::js($id, $cols, $ajaxSrcUrl, $defaultLength, $lengthMenu, $tableName, $sortTable);
        $output .= self::html($id, $cols, $data);
        return $output;
    }
}
