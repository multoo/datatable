<?php

namespace Multoo\DataTable\Handler\Framework;

use Multoo\DataTable\Helper\Template;

class Pdo extends \Multoo\DataTable\AbstractHelper
{

    /**
     *
     * @var \Framework\PdoDatabase
     */
    protected $pdo;
    protected $query;
    protected $bind = "";
    protected $pdoFrom;
    protected $pdoWhere;
    protected $pdoBind;

    public function __construct(\Framework\PdoDatabase $pdo, $from, $where = "", $bind = "")
    {
        $this->pdo = $pdo;
        $this->pdoFrom = $from;
        $this->pdoWhere = $where;
        $this->pdoBind = $bind;

        return $this;
    }

    protected function query($query, $bind = "")
    {
        $this->query = $query;
        $this->bind = $bind;

        return $this;
    }

    protected function makeQuery($from, $where = "", $bind = "")
    {
        $this->query = "SELECT \n";

        $tmpArray = array();

        if (!empty($this->extraFields)) {
            $tmpArray[] = $this->extraFields;
        }

        foreach ($this->columns as $column) {
            if (isset($column['dbField'])) {
                $tmp = $column['dbField'];
                if (!empty($column['name'])) {
                    $tmp .= " AS " . $column['name'];
                }

                $tmpArray[] = $tmp;
            }
        }

        if (empty($where)) {
            $where = "1 = 1";
        }

        $this->query .= implode(', ', $tmpArray);
        $this->query .= " FROM " . $from;
        $this->query .= " WHERE " . $where;

        $this->bind = $bind;

        return $this;
    }

    protected function order()
    {
        $this->order = "";
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                $sortCol = $this->columns[intval($_GET['iSortCol_' . $i])]['name'] ?? $this->columns[intval($_GET['iSortCol_' . $i])]['dbField'] ?? null;
                if ($sortCol !== null && $_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    if (!empty($this->order)) {
                        $this->order .= ", ";
                    }

                    $this->order .= $sortCol . " " . (($_GET['sSortDir_' . $i] == 'desc') ? 'DESC' : 'ASC');
                }
            }
        }

        return $this;
    }

    protected function limit()
    {
        $this->limit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $this->limit = "" . int($_GET['iDisplayStart']) . ", " . int($_GET['iDisplayLength']);
        }

        return $this;
    }

    protected function where()
    {
        $this->where = "";
        for ($i = 0; $i < count($this->columns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] !== '') {
                if (!empty($this->where)) {
                    $this->where .= " AND ";
                }

                if ($_GET['sSearch_' . $i] == 'notNull') {
                    $this->where .= $this->columns[$i]['dbField'] . " IS NOT null ";
                } elseif ($_GET['sSearch_' . $i] == 'null') {
                    $this->where .= $this->columns[$i]['dbField'] . " IS null ";
                } elseif ($_GET['sSearch_' . $i] == 'notEmpty') {
                    $this->where .= $this->columns[$i]['dbField'] . " NOT IN ('','0000-00-00','0000-00-00 00:00:00') ";
                } elseif ($_GET['sSearch_' . $i] == 'empty') {
                    $this->where .= $this->columns[$i]['dbField'] . " IN ('','0000-00-00','0000-00-00 00:00:00') ";
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'date') {
                    $this->where .= $this->columns[$i]['dbField'] . " = '" . dateConverter($_GET['sSearch_' . $i]) . "' ";
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'wildcardLeft') {
                    $this->where .= $this->columns[$i]['dbField'] . " LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "' ";
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'wildcardRight') {
                    $this->where .= $this->columns[$i]['dbField'] . " LIKE '" . addslashes($_GET['sSearch_' . $i]) . "%' ";
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'exactMatch') {
                    $this->where .= $this->columns[$i]['dbField'] . " = '" . addslashes($_GET['sSearch_' . $i]) . "' ";
                } elseif (!isset($this->columns[$i]['filter']) || $this->columns[$i]['filter'] == 'wildcard') {
                    $this->where .= $this->columns[$i]['dbField'] . " LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
                }
            }

        }
        if (isset($_GET['sSearch']) && $_GET['sSearch'] !== '') {
            if (!empty($this->where)) {
                $this->where .= " AND ";
            }
            $this->where .= " (";
            for ($i = 0; $i < count($this->columns); $i++) {
                if ($i !== 0) {
                    $this->where .= " OR ";
                }

                $this->where .= $this->columns[$i]['dbField'] . " LIKE '%" . addslashes($_GET['sSearch']) . "%' ";
            }
            $this->where .= ")";
        }

        return $this;
    }

    protected function data()
    {
        $this->makeQuery($this->pdoFrom, $this->pdoWhere, $this->pdoBind);

        $this->where()->order()->limit();

        $this->total = $this->pdo->numRows($this->query, $this->bind);

        if (stripos($this->query, 'WHERE') === false) {
            $where = (!empty($this->where)) ? ' WHERE ' . $this->where : '';
        } else {
            $where = (!empty($this->where)) ? ' AND ' . $this->where : '';
        }

        $order = (!empty($this->order)) ? ' ORDER BY ' . $this->order : '';
        $limit = (!empty($this->limit)) ? ' LIMIT ' . $this->limit : '';

        if (!empty($this->where)) {
            if (substr_count($this->query, 'SELECT') === 1 && stripos($this->query, 'GROUP BY') !== false) {
                $queryAndWhere = str_replace('GROUP BY', $where . ' GROUP BY', $this->query);
            } else {
                $queryAndWhere = $this->query . $where;
            }

            $this->totalDisplayRecords = $this->pdo->numRows($queryAndWhere, $this->bind);
            $this->data = $this->pdo->getArray($queryAndWhere . $order . $limit, $this->bind);
        } else {
            $this->data = $this->pdo->getArray($this->query . $order . $limit, $this->bind);
            $this->totalDisplayRecords = $this->total;
        }

        return $this;
    }

    protected function rows()
    {
        $this->data();

        Template::clearCache();

        foreach ($this->data as $line) {
            $row = array();

            foreach ($this->columns as $index => $column) {
                if (isset($column['template'])) {
                    $row[] = Template::parseRow('col' . $index, $column['template'], $line);
                } else {
                    if (isset($column['data'])) {
                        $row[] = $column['data'];
                    } elseif (isset($column['name'])) {
                        $row[] = $line[$column['name']];
                    } elseif (isset($column['dbField']) && stripos($column['dbField'], "(") === false) {
                        if (strpos($column['dbField'], ".") === false) {
                            $row[] = $line[$column['dbField']];
                        } else {
                            $row[] = $line[substr(strrchr($column['dbField'], "."), 1)];
                        }
                    }
                }
            }

            $this->rows[] = $row;
        }
    }
}
