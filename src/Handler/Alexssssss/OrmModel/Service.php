<?php

namespace Multoo\DataTable\Handler\Alexssssss\OrmModel;

use Alexssssss\OrmModel\Exception\Limit;
use Multoo\DataTable\Helper\Template;

class Service extends \Multoo\DataTable\AbstractHelper
{

    /**
     * @var \Alexssssss\OrmModel\ObjectStorage
     */
    protected $data = null;

    /**
     * @var \Alexssssss\OrmModel\Service\ServiceInterface
     */
    protected $service;
    protected $function;
    protected $bind = [];
    protected $with = false;

    public function __construct(\Alexssssss\OrmModel\Service\ServiceInterface $service, $function = "get", $where = "", array $bind = [], $with = false)
    {
        $this->service = $service;
        $this->function = $function;
        $this->where = $where;
        $this->bind = $bind;
        $this->with = $with;

        return $this;
    }

    protected function rows()
    {
        $this->data();

        Template::clearCache();

        foreach ($this->data as $entity) {
            $row = [];

            foreach ($this->columns as $index => $column) {
                if (isset($column['template'])) {
                    $row[] = Template::parseRow('col' . $index, $column['template'], $entity);
                } else {
                    if (isset($column['data'])) {
                        $row[] = $column['data'];
                    } else {
                        if (isset($column['name'])) {
                            $row[] = $entity->{$column['name']};
                        } elseif (isset($column['entityField'])) {
                            $parts = explode('->', $column['entityField']);
                            $entityPath = $entity;
                            foreach ($parts as $part) {
                                if (substr($part, -2) === '()') {
                                    $entityPath = $entityPath->{substr($part, 0, -2)}();
                                } elseif (($char = strpos($part, '(')) !== false) {
                                    $functie = substr($part, 0, $char);
                                    $arguments = explode(",", substr($part, $char, -1));
                                    $entityPath = call_user_func_array([$entityPath, $functie], $arguments);
                                } else {
                                    $entityPath = $entityPath->{$part};
                                }
                            }
                            $row[] = $entityPath;
                        }
                    }
                }
            }

            $this->rows[] = $row;
        }
    }

    protected function data()
    {
        if ($this->with == false) {
            $this->data = $this->service->{$this->function}($this->where, $this->bind);
        } else {
            $this->data = $this->service->with($this->with)->{$this->function}($this->where, $this->bind);
        }

        $this->total = $this->data->count();

        $this->where()->order()->limit();

        return $this;
    }

    protected function limit()
    {
        if (!isset($_GET['iDisplayLength']) || $_GET['iDisplayLength'] != '-1') {
            $this->data = $this->data->limit((int)($_GET['iDisplayStart'] ?? 0), (int)($_GET['iDisplayLength'] ?? 1000));
        }

        return $this;
    }

    protected function order()
    {
        if (isset($_GET['iSortCol_0'])) {
            if (isset($this->columns[(int)$_GET['iSortCol_0']]['entityField']) && $_GET['bSortable_' . $_GET['iSortCol_0']] == "true") {
                $sortBy = [$this->columns[(int)$_GET['iSortCol_0']]['entityField'] => strtoupper($_GET['sSortDir_0'])];
                $this->data->sort($sortBy);
            }
        }

        return $this;
    }

    protected function where()
    {
        $search = [];
        $searchRegex = [];

        for ($i = 0; $i < count($this->columns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] !== '') {
                if ($_GET['sSearch_' . $i] == 'notNull') {
                    $search[] = [$this->columns[$i]['entityField'], '<>', null];
                } elseif ($_GET['sSearch_' . $i] == 'null') {
                    $search[] = [$this->columns[$i]['entityField'] => null];
                } elseif ($_GET['sSearch_' . $i] == 'notEmpty') {
                    $search[] = [$this->columns[$i]['entityField'], '<>', ['', '0000-00-00', '0000-00-00 00:00:00']];
                } elseif ($_GET['sSearch_' . $i] == 'empty') {
                    $search[] = [$this->columns[$i]['entityField'] => ['', '0000-00-00', '0000-00-00 00:00:00']];
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'date') {
                    $search[] = [$this->columns[$i]['entityField'] => dateConverter($_GET['sSearch_' . $i])];
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'wildcardLeft') {
                    $searchRegex[] = [$this->columns[$i]['entityField'] => '/.*' . quotemeta($_GET['sSearch_' . $i]) . '/i'];
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'wildcardRight') {
                    $searchRegex[] = [$this->columns[$i]['entityField'] => '/' . quotemeta($_GET['sSearch_' . $i]) . '.*/i'];
                } elseif (isset($this->columns[$i]['filter']) && $this->columns[$i]['filter'] == 'exactMatch') {
                    $search[] = [$this->columns[$i]['entityField'] => $_GET['sSearch_' . $i]];
                } elseif (!isset($this->columns[$i]['filter']) || $this->columns[$i]['filter'] == 'wildcard') {
                    $searchRegex[] = [$this->columns[$i]['entityField'] => '/.*' . quotemeta($_GET['sSearch_' . $i]) . '.*/i'];
                }
            }
        }

        if (!empty($search)) {
            $this->data = $this->data->get($search);
        }

        if (!empty($searchRegex)) {
            $this->data = $this->data->get($searchRegex, true);
        }

        $this->totalDisplayRecords = $this->data->count();

        return $this;
    }
}
