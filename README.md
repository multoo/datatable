DataTable
====
| Branch  | GitLab CI Status                                                                                                                                                                                                                                                              |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Master  | [![build status](https://gitlab.com/multoo/datatable/badges/master/build.svg)](https://gitlab.com/multoo/datatable/commits/master) [![coverage report](https://gitlab.com/multoo/datatable/badges/master/coverage.svg)](https://gitlab.com/multoo/datatable/commits/master)   |
